# portfolioAmira

## presentation de projet portfolio
  Dès notre arrivé à Simplon pour suivre la formation ‘Développement-Web’, nous avons suivi des cours HTML, CSS et Bootstrap. Chaque cours est suivi par des sessions de travaux pratiques pour mieux maîtriser les notions théoriques . Pour mieux maîtriser ces concepts, chacun d’entre nous a réalisé un projet personnel qui consiste à faire un portfolio contenant toutes les informations nécessaires. 

## la maquette 
![wireframe portfolio](image/MaquetteAmira.png)



## le lien de gitlab:
https://amira93.gitlab.io/portfolioamira


